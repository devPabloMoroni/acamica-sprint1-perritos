let array_perritos = [];

class perro{
    constructor(nombre,raza,edad,peso,estado_adopcion){
        this.nombre = nombre;
        this.raza = raza;
        this.edad = edad;
        this.peso = peso;
        this.estado_adopcion = estado_adopcion;
    }

    modificar_adopcion(opcion){
        if(opcion == 1){
            this.estado_adopcion = "En adopcion";
        }
        if(opcion == 2){
            this.estado_adopcion = "En proceso de adopcion";
        }
        if(opcion == 3){
            this.estado_adopcion = "Adoptado";
        }
    }

    informa_estado_adopcion(){
        return this.nombre + " esta " + this.estado_adopcion;
    }

};

function agregar_perrito(nombre,raza,edad,peso,estado_adopcion){
    perrito = new perro(nombre,raza,edad,peso,estado_adopcion);
    array_perritos.push(perrito);
}

function mostrar_perritos(){
    console.log(array_perritos);
}

function mostrar_por_estado(estado){
    let listado_por_estado = array_perritos.filter(function(objeto){
        if (objeto.estado_adopcion == estado){
            return objeto;
        }
    });

    console.log("\n" + estado + ": " + listado_por_estado.length);
    listado_por_estado.forEach(function(array_perritos){
        console.log(array_perritos);
    });
}


//METODO PARA MODIFICAR ESTADO DE ADOPCION
console.log("---------Estado inicial del perrito---------")
perro1 = new perro('tango','ovejero',9,29,"En adopcion");
console.log(perro1);
console.log("---------Cambio el estado (1 en adopcion, 2 en proceso, 3 adoptados---------")
perro1.modificar_adopcion(3);
console.log(perro1.informa_estado_adopcion());
agregar_perrito(perro1);

console.log("---------Perritos Ingresados por el usuario---------")